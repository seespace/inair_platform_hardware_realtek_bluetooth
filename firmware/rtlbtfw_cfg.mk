PRODUCT_COPY_FILES += \
                $(LOCAL_PATH)/rtl8723a_fw:system/etc/firmware/rtl8723a_fw \
                $(LOCAL_PATH)/rtl8723a_config:system/etc/firmware/rtl8723a_config \
                $(LOCAL_PATH)/rtl8723b_fw:system/etc/firmware/rtl8723b_fw \
                $(LOCAL_PATH)/rtl8723b_config:system/etc/firmware/rtl8723b_config \
                $(LOCAL_PATH)/rtl8761a_fw:system/etc/firmware/rtl8761a_fw \
                $(LOCAL_PATH)/rtl8761a_config:system/etc/firmware/rtl8761a_config \
                $(LOCAL_PATH)/rtl8821a_fw:system/etc/firmware/rtl8821a_fw   \
                $(LOCAL_PATH)/rtl8821a_config:system/etc/firmware/rtl8821a_config \
                $(LOCAL_PATH)/rtl8723a_fw:system/etc/firmware/mp_rtl8723a_fw \
                $(LOCAL_PATH)/rtl8723b_fw:system/etc/firmware/mp_rtl8723b_fw \
                $(LOCAL_PATH)/rtl8761a_fw:system/etc/firmware/mp_rtl8761a_fw \
                $(LOCAL_PATH)/rtl8821a_fw:system/etc/firmware/mp_rtl8821a_fw
